FOREGROUND_TEXT_COLOR = '#FFFFFF'
INACTIVE_TEXT_COLOR = '#333333'
BACKGROUND_COLOR = '#000000'
ECI_GRAPH_COLORS = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00', 
                    '#ffff33']
MC_MEAN_CURVE_COLOR = '#8da0cb'

# Modes for concentration in MC
SYSTEMS_FROM_DB = 0
CONC_PER_BASIS = 1