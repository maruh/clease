.. CLEASE documentation master file, created by
   sphinx-quickstart on Fri Aug 16 15:58:30 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CLEASE's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   clease
   ce_aucu
   montecarlo_sampling



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
