.. _ce_aucu_tutorial:


================================
Cluster Expansion on AuCu alloys
================================
.. toctree::
  :maxdepth: 2

  ./aucu_concentration
  ./aucu_setting
  ./aucu_initial_pool
  ./aucu_run_calculations
